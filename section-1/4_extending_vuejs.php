<?php 
require_once('../header.php');

?>

	<div id="app">
		<input type="text" name="name" v-on:input="changeTitle" placeholder="Enter your name">
		<p> {{ title }}</p>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			title : "Hello World"
		},
		methods : {
			changeTitle: function(event){
				this.title = event.target.value;
			}
		}
	});
</script>

