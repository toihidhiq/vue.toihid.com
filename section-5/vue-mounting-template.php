<?php 
require_once('../header.php');
?>
	<div id="app" class="text-center">
		<h2 > {{ title }}</h2>
	</div>
	<div id="app2" class="text-center">
		
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	var vm1 = new Vue({
		data: {
			title : "Vue Instance"
		}
	});
	vm1.$mount("#app");

	var vm2 = new Vue({
		template : "<h2> Hi Vue template</h2>"
	});
	vm2.$mount("#app2");
</script>

