<?php 
require_once('../header.php');
?>
<div id="example" class="text-center">
  <my-component></my-component>
  <my-component></my-component>
</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	// register
	Vue.component('my-component', {
	  template: '<div>A custom component!</div>'
	});

	// create a root instance
	new Vue({
	  el: '#example'
	})

</script>

