<?php 
require_once('../header.php');

?>

	<div id="app">
		<p v-if="show"> You cane see me </p>
		<p v-else> I am from else  </p>
		<button @click="show =! show"> Switch  </button>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			show : true,
		}
	});
</script>

