<?php 
require_once('../header.php');

?>
	<div id="app">
		<p v-show="show"> You cane see me </p>
		<p v-else> I am from else  </p>
		<template v-if="show">	
				<h2> Hi Template</h2>
				<p>	I am from template</p>
		</template>
		<button @click="show =! show"> Switch  </button>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			show : true,
		}
	});
</script>

