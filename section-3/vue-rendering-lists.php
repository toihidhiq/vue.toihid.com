<?php 
require_once('../header.php');

?>
	<div id="app">
		<h3> Item</h3>
        <ul>
        	<li v-for="country in countries"> {{ country }}</li>
        </ul>

        <h3> Item and Index </h3>
        <ul>
        	<li v-for="(country, i ) in countries"> {{ country }} ({{ i }})</li>
        </ul>

        <h3> Item in template </h3>
        <template v-for="(country, index ) in countries">	
        	<h5> {{ country }} </h5>
        	<h6>  {{ index }}</h6>
        </template>

        <h3> Items by object </h3>
        <li v-for="val in persons"> {{ val.name }}</li>

        <h3> Items value, key and index </h3>
        <div v-for="person in persons">
        		<div v-for="(value, key, index ) in person"> {{ key }} : {{ value }}  ({{ index }})</div>
        		<br/>
        </div>

        <h3> Loop and numberig </h3>
        <ul >
        		<i v-for="n in 10"> {{ n }}</i>
        </ul>

        <h3> Push elements  </h3>
        <button @click="countries.push('Norway')">Add Country</button>

	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
		countries : [ 'Bangladesh','Sweden', 'USA', 'Canada' ],
		persons : [
				{ 'name' : 'Max', 'age': 33, 'country': 'Sweden' },
				{ 'name' : 'Maxi', 'age': 32, 'country': 'Canada' }
				]
		}
	});
</script>

