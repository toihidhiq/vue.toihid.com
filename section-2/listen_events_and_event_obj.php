<?php 
require_once('../header.php');

?>

	<div id="app">
		<button v-on:click="Count">Click</button>
		<p>{{ count }} </p>
		<p v-on:mousemove="UpdateCondinates"> Mouse over here.</p>
		<p>Cordinates {{ x }} - {{ y }} </p>

	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			count : 0,
			x : 0,
			y : 0
		},
		methods : {
			Count: function(){
				this.count ++;
			},
			UpdateCondinates : function(event){
				this.x = event.clientX;
				this.y = event.clientY;
			}
		}

	});
</script>

