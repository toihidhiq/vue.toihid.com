<?php 
require_once('../header.php');

?>

	<div id="app">
		<button v-on:click="count++"> Increment</button>
		<button v-on:click="count--"> Decrement</button>
		<p> Count: {{ count }}</p>
		<button v-on:click="count2++"> Increment Count 2</button>
		<p> Count2: {{ count2 }}</p>
		<p class="color-red"> Odd / even for count :   {{ output }} - {{ result() }} </p>		


	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data:{
			count : 0,
			count2 : 0
		},
		computed:{
			output: function(){
				console.log('computed');
				return this.count % 2 == 0 ? 'Even' : 'Odd' ; 
			}
		},
		methods:{
			result: function(){
				console.log('methods');
				return this.count % 2 == 0 ? 'Even' : 'Odd' ; 
			}
		}		

	});
</script>

