<?php 
require_once('../header.php');

?>

	<div id="app">
		<p> {{ changeTitle() }}</p>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			title : "Hello World"
		},
		methods : {
			changeTitle: function(event){
				return this.title;
			}
		}
	});
</script>

