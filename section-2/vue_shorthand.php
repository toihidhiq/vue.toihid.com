<?php 
require_once('../header.php');

?>

	<div id="app">
		<button v-on:click="Count(1)">Increment by 1 </button>
		<p>Count : {{ count}}</p>
		<a v-bind:href="link">Link</a>
		<br/>
		<button @click="Count(1)">Increment by 1 </button>
		<p>Count : {{ count}}</p>
		<a :href="link">Link</a>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			count : 0,
			link : "http://vue.toihid.com/"
		},
		methods : {
			Count: function(step){
				this.count += step;
			}
		}

	});
</script>

