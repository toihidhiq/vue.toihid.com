<?php 
require_once('../header.php');
?>

<style type="text/css">
	.demo{
		width: 200px;
		height: 200px;
		border: 1px solid grey;
		margin: 20px;
	}
	.red{
		background-color: red;
	}
	.blue{
		background-color: blue;
	}
	.green{
		background-color: green;
	}	
</style>

	<div id="app">
		<div class="demo" :style="{ backgroundColor : color}"> Click here</div>
		<div class="demo" :style="style"></div>	
		<div class="demo" ></div>
		<div>
			Enter color <input type="text" name="" v-model="color">
		</div>
		<div>
			Enter width <input type="text" name="" v-model="width">
		</div>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			color: 'grey',
			width: 200,
		},
		computed: {
			style : function () {
				return {
					backgroundColor : this.color,
					width : this.width + 'px'
				}
			}

		}

	});
</script>

