<?php 
require_once('../header.php');
?>

<style type="text/css">
	.demo{
		width: 200px;
		height: 200px;
		border: 1px solid grey;
		margin: 20px;
	}
	.red{
		background-color: red;
	}
	.blue{
		background-color: blue;
	}
</style>

	<div id="app">
		<div class="demo" @click="attachedRed =! attachedRed" :class="divClasses"> Click here</div>
		<div class="demo" :class="{ red: attachedRed }"></div>	
		<div class="demo"></div>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			attachedRed : false
		},
		computed: {
			divClasses : function () {
				return {
					red : this.attachedRed,
					blue : !this.attachedRed
				}
			}

		}

	});
</script>

