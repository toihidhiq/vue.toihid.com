<?php 
require_once('../header.php');

?>

	<div id="app">
		<p> {{ changeTitle() }} - <a v-bind:href="link"> Vue Tutorials</a></p>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			title : "Hello World",
			link : "http://vue.toihid.com/"
		},
		methods : {
			changeTitle: function(event){
				return this.title;
			}
		}
	});
</script>

