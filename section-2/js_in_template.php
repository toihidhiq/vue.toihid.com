<?php 
require_once('../header.php');

?>

	<div id="app">
		<button v-on:click="Count(1)">Increment by 1 </button>
		<button v-on:click="count +=2">Increment by 2</button>
		<p>Count : {{ count}}</p>
		<p>Even / odd : {{ count % 2 == 0 ? 'Even' : 'odd' }}</p>
		<p>Count * 3 :  {{ count * 3}} </p>

	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			count : 0
		},
		methods : {
			Count: function(step){
				this.count += step;
			}
		}

	});
</script>

