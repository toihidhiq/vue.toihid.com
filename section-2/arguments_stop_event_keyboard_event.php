<?php 
require_once('../header.php');

?>

	<div id="app">
		<button v-on:click="Count(2)">Click</button>
		<p>{{ count }} </p>
		<p v-on:mousemove="UpdateCondinates"> Mouse over here. <span v-on:mousemove.stop="" class="color-red"> Stop Calculating</span></p>

		<p>Cordinates {{ x }} - {{ y }} </p>
		<input type="text" name="" v-on:keyup.enter.space="Words">
		<p> Number of space: {{ wordCount }}</p>

	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			count : 0,
			x : 0,
			y : 0,
			wordCount: 0
		},
		methods : {
			Count: function(step){
				this.count += step;
			},
			UpdateCondinates : function(event){
				this.x = event.clientX;
				this.y = event.clientY;
			},
			Words : function(event){
				this.wordCount +=1;
			}
		}

	});
</script>

