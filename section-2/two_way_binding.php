<?php 
require_once('../header.php');

?>

	<div id="app">
		<input type="text" placeholder="Enter your name" v-model="name">
		<p>Name: {{ name}}</p>
	</div>

<?php 
require_once('../footer.php');
?>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			name : '',
		}
	});
</script>

