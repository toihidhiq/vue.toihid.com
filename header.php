<?php $base_rul =  "http://" . $_SERVER['SERVER_NAME'] ; ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Simple Sidebar - Start Bootstrap Template</title>


  <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo $base_rul; ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo $base_rul; ?>/css/simple-sidebar.css" rel="stylesheet">




</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Start Vue JS</div>
      <div class="list-group list-group-flush">
        <h5>Section 1</h5>
        <a href="<?php echo $base_rul; ?>/section-1/4_extending_vuejs.php" class="list-group-item list-group-item-action bg-light"> Extending Vue js</a>
        <h5>Section 2</h5>
        <a href="<?php echo $base_rul; ?>/section-2/function_access_data.php" class="list-group-item list-group-item-action bg-light">Function and access data</a>
        <a href="<?php echo $base_rul; ?>/section-2/directives_properties.php" class="list-group-item list-group-item-action bg-light">Reactive Properties</a>
        <a href="<?php echo $base_rul; ?>/section-2/disable_re_rendering.php" class="list-group-item list-group-item-action bg-light">Disable re-rendering</a>
        <a href="<?php echo $base_rul; ?>/section-2/listen_events_and_event_obj.php" class="list-group-item list-group-item-action bg-light">Listen to events and event obj</a>
        <a href="<?php echo $base_rul; ?>/section-2/arguments_stop_event_keyboard_event.php" class="list-group-item list-group-item-action bg-light">Arguments and Event operations</a>
        <a href="<?php echo $base_rul; ?>/section-2/js_in_template.php" class="list-group-item list-group-item-action bg-light">Write Javascript in Template</a>
        <a href="<?php echo $base_rul; ?>/section-2/two_way_binding.php" class="list-group-item list-group-item-action bg-light">Two way binding</a>
        <a href="<?php echo $base_rul; ?>/section-2/reacting_with_computed_properties.php" class="list-group-item list-group-item-action bg-light"> Reacting to changes with computed properties</a>
        <a href="<?php echo $base_rul; ?>/section-2/watch_alternative_computed_propeerties.php" class="list-group-item list-group-item-action bg-light"> Watch , alternative use of computed</a>
        <a href="<?php echo $base_rul; ?>/section-2/vue_shorthand.php" class="list-group-item list-group-item-action bg-light"> Shorthands</a>
        <a href="<?php echo $base_rul; ?>/section-2/vue_dynamic_style_by_class.php" class="list-group-item list-group-item-action bg-light"> Dynamic design by class</a>
        <a href="<?php echo $base_rul; ?>/section-2/vue_dynamic_style_by_objects.php" class="list-group-item list-group-item-action bg-light"> Dynamic design by objects</a>
        <a href="<?php echo $base_rul; ?>/section-2/vue_dynamic_style_by_name.php" class="list-group-item list-group-item-action bg-light"> Dynamic design by name</a>
        <a href="<?php echo $base_rul; ?>/section-2/vue_dynamic_style_without_css_class.php" class="list-group-item list-group-item-action bg-light"> Dynamic design without css class</a>
        <h5>Section 3</h5>
        <a href="<?php echo $base_rul; ?>/section-3/vue-conditional-rendering.php" class="list-group-item list-group-item-action bg-light"> Vue conditional rendering</a>
        <a href="<?php echo $base_rul; ?>/section-3/vue-conditional-rendering-group.php" class="list-group-item list-group-item-action bg-light"> Vue conditional rendering by template</a>
        <a href="<?php echo $base_rul; ?>/section-3/vue-conditional-rendering-detach.php" class="list-group-item list-group-item-action bg-light"> Vue conditional rendering by detaching</a>
        <a href="<?php echo $base_rul; ?>/section-3/vue-rendering-lists.php" class="list-group-item list-group-item-action bg-light"> Vue rendering lists</a>
        
        <h5>Section 4</h5>
        <a href="<?php echo $base_rul; ?>/section-4/exercise/index.php" class="list-group-item list-group-item-action bg-light">Monster Game Exercise</a>

        <h5>Section 5 vue instance</h5>
        <a href="<?php echo $base_rul; ?>/section-5/vue-mounting-template.php" class="list-group-item list-group-item-action bg-light">Mounting Template</a>
        <a href="<?php echo $base_rul; ?>/section-5/vue-components.php" class="list-group-item list-group-item-action bg-light">Component</a>
        


        <a href="#" class="list-group-item list-group-item-action bg-light">Status</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>